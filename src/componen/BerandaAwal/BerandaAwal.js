import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import '../css/bootstrap.min.css';
import '../css/bootstrap-theme.min.css';
import '../css/fullcalendar.css';
import '../css/fullcalendar.print.css';
import '../css/jasny-bootstrap.min.css';
import '../css/navmenu-reveal.css';
import '../css/responsive-style.css';
import './BerandaAwal.css';

import Dasboard from '../img/3---Dashboard---header.png';
import DashPerizinan from '../img/3---Dashboard---User_dash1perizinanonline.png';
import DashMonitoring from '../img/3---Dashboard---User_dash2monitoringberkas.png';
import DashPenggunaan from '../img/3---Dashboard---User_dash3crapenggunaan.png';
import DashCC from '../img/3---Dashboard---User_dash4callcenter.png';
import DashRegulasi from '../img/3---Dashboard---User_dash5regulasi.png';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBars } from '@fortawesome/free-solid-svg-icons'

export default function BerandaAwal(App) {
    return (
        <div>
            <nav class="mnb navbar navbar-default navbar-fixed-top">
                <div class="container-fluid">
                    <div class="navbar-header">


                        <div >
                            <a href="#" id="msbo"><FontAwesomeIcon icon={faBars} /></a><img src={Dasboard} alt="" />
                        </div>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#"><FontAwesomeIcon icon={faBars} /> Hubungi</a></li>
                            <li><a href="#"><FontAwesomeIcon icon={faBars} /> Tentang</a></li>
                            <li><a href="#"><FontAwesomeIcon icon={faBars} /> Bantuan</a></li>
                            <li><a href="#"><FontAwesomeIcon icon={faBars} /><span class="text">LOGOUT</span></a></li>
                        </ul>

                    </div>

                </div>



            </nav>

            <div class="msb" id="msb">
                <nav class="navbar navbar-default" role="navigation">

                    <div class="navbar-header">
                        <div class="brand-wrapper">

                            <div class="navbar-header dashboard">


                                <div >
                                    <h4>DASHBOARD MENU <a href="#" id="mbo"><i class="ic fa fa-bars"></i> </a></h4>

                                </div>


                            </div>

                        </div>
                    </div>

                    <div class="side-menu-container">
                        <ul class="nav navbar-nav">
                            <li><a href="#"> <h5 > <img src={DashPerizinan} alt="" /> Perizinan Online</h5></a> </li>
                            <li><a href="#"><h5 ><img src={DashMonitoring} alt="" /> Monitoring Berkas</h5></a></li>
                            <li><a href="#"><h5 ><img src={DashPenggunaan} alt="" /> Cara Penggunaan</h5></a></li>
                            <li><a href="#"><h5 ><img src={DashCC} alt="" /> Call Center</h5></a></li>
                            <li><a href="#"><h5 ><img src={DashRegulasi} alt="" /> Regulasi/Ketentuan</h5></a></li>
                        </ul>
                    </div>
                </nav>
            </div>



            <div class="mcw">
                <div class="row" >
                    <div class="col-md-12" >
                        <div class="atas">
                            <h3>DASHBOARD</h3>

                            <div class="col-md-3 col-xs-12" >
                                <div class="search-dashboard">
                                    <div class="input-group">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="#"></i></button>
                                        </span>
                                        <input type="text" class="form-control" placeholder="Search people, documents, dates..." />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6" >
                        <div class="grey-box">
                            <h4>Selamat Datang <br />di Halaman Dashboard Anda, User</h4>
                            <hr />
                            <p>Gunakan halaman ini untuk mengajukan Surat Pengajuan Berkas-berkas kendaraan Anda.</p>
                            <p>Kami menyarankan Anda untuk mempersiapkan berkas-berkas yang akan diupload terlebih dahulu sebelum menggunakan aplikasi ini.</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12 col-sm-6" >
                        <div class="white-box">
                            <h4>Tahapan pengajuan berkas</h4>
                            <ul class="list-step">
                                <li><i class="icon icon-list one"></i>
                                    <div class="desc">
                                        <p><b>Pendaftaran Akun</b></p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                    </div>
                                </li>
                                <li><i class="icon icon-list two"></i>
                                    <div class="desc">
                                        <p><b>Permohonan Persetujuan Izin</b></p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                    </div>
                                </li>
                                <li><i class="icon icon-list three"></i>
                                    <div class="desc">
                                        <p ><b>Permohonan Realisasi SK</b></p>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>


                    <div class="col-md-12">
                        <div class="bawah">
                            <h4><b>DOWNLOAD USER GUIDE</b></h4>
                            <ul>
                                <a><i class="fa fa-caret-right" aria-hidden="true"></i> <span class="text">Pendaftaran Akun</span></a> <br />
                                <a><i class="fa fa-caret-right" aria-hidden="true"></i> <span class="text">Permohonan Persetujuan Izin/izin Prinsip</span></a> <br />
                                <a><i class="fa fa-caret-right" aria-hidden="true"></i> <span class="text">Permohonan Realisasi SK, Izin Penyelenggaraan dan Kartu Pengawasan</span></a>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" >
                    <div class="footer">
                        <p>Hak Cipta &copy; 2018 . <strong>Badan Pengelola Transportasi JABODETABEK</strong>  | All Rights Reserved.</p>
                    </div>
                </div>

            </div>

        </div>
    )
}

