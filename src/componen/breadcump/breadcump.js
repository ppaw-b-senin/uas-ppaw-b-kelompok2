import React from 'react';
import '../css/bootstrap-theme.css';
import '../css/bootstrap-theme.css.map';
import '../css/bootstrap-theme.min.css';
import '../css/bootstrap-theme.min.css.map';
import '../css/bootstrap.css';
import '../css/bootstrap.css.map';
import '../css/bootstrap.min.css';
import '../css/bootstrap.min.css.map';
import '../App.css';


export default function breadcump () {
    return (
      <div class="mcw">
        <div class="row" >
            <div class="col-md-12" >
                <div class="atas">
                    <h3>DASHBOARD</h3>
                    <div class="breadcrumb-info">
                      <i class="fa fa-home" aria-hidden="true"></i> <a href="#">Home</a> <span class="divider">/</span> <a href="#" class="active">Dashboard</a>
                    </div>
                    <div class="col-md-3 col-xs-12" >
                      <div class="search-dashboard">
                        <div class="input-group">
                          <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-search" aria-hidden="#"></i></button>
                          </span>
                          <input type="text" class="form-control" placeholder="Search people, documents, dates..."/>
                        </div>
                      </div>
                    </div>  
                </div>     
            </div> 
         </div>
        </div>
    );
}