import React, { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import '../css/bootstrap.min.css';
import '../css/bootstrap-theme.min.css';
import '../css/fullcalendar.css';
import '../css/fullcalendar.print.css';
import '../css/jasny-bootstrap.min.css';
import '../css/navmenu-reveal.css';
import '../css/responsive-style.css';
import '../css/style.css';
import ReCAPTCHA from "react-google-recaptcha";
import { FormGroup, FormControl } from "react-bootstrap"
import Carousel from 'react-bootstrap/Carousel';
import GambarAwal from '../img/reg-slider.jpg';
import Register from '../Register/Register';


const recaptchaRef = React.createRef();


export default function Login(App) {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    function validateForm() {
        return email.length > 0 && password.length > 0;
    }

    function handleSubmit(event) {
        event.preventDefault();
    }

    return (
        //     <Router>
        //   <div>
        //     <OldSchoolMenuLink
        //       activeOnlyWhenExact={true}
        //       to="/"
        //       label="Home"
        //     />
        //     <OldSchoolMenuLink to="/about" label="About" />

        //     <hr />

        //     <Switch>
        //       <Route exact path="/">
        //         <Home />
        //       </Route>
        //       <Route path="/about">
        //         <About />
        //       </Route>
        //     </Switch>
        //   </div>
        // </Router>

        <div id="inner-login">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-xs-12 col-sm-4">
                        <div class="registration-form">
                            <form class="" action="index.html" method="post" onSubmit={handleSubmit}>
                                <h2 class="title">Masuk Ke Perijinan Online BPTJ</h2>
                                <FormGroup controlId="email" bsSize="large">
                                    <label for="password">Username</label>
                                    <FormControl
                                        autoFocus
                                        type="email"
                                        value={email}
                                        placeholder="Masukan Email atau Username"
                                        onChange={e => setEmail(e.target.value)}
                                    />
                                </FormGroup>
                                <FormGroup controlId="password" bsSize="large">
                                    <label for="password">Password</label>
                                    <FormControl
                                        value={password}
                                        placeholder="Masukan Password"
                                        onChange={e => setPassword(e.target.value)}
                                        type="password"
                                    />
                                </FormGroup>
                                <form onSubmit={() => { recaptchaRef.current.execute(); }}>
                                    <ReCAPTCHA
                                        ref={recaptchaRef}
                                        sitekey="Your client site key"
                                    />
                                </form>
                                <button block bsSize="large" enabled={!validateForm()} type="submit" class="btn btn-default login" >Login</button>
                                <button block bsSize="large" enabled={!validateForm()} type="button" 
                                onClick={event =>  window.location.href='../Register/Register.js'}
                                    class="btn btn-default reg">REGISTRASI</button>
                                <div class="footer-form">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" class="custom-control-input" id="customCheck1" />
                                        <label class="custom-control-label" for="customCheck1">Remember Me</label>
                                    </div>
                                    <div class="link">
                                        <a href="#">Forgot Password?</a>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-8 col-xs-12 col-sm-8">
                        <div class="slide-home">
                            <div id="Reg_slide" class="slider-reg">
                                <div class="owl-carousel reg-home owl-theme">
                                    <Carousel>
                                        <Carousel.Item>
                                            <img
                                                src={GambarAwal}
                                            />
                                        </Carousel.Item>
                                        <Carousel.Item>
                                            <img
                                                src={GambarAwal}
                                            />
                                        </Carousel.Item>
                                    </Carousel>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
