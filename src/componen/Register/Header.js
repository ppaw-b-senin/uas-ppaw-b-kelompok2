import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import './assets/css/bootstrap.min.css';
import './assets/css/bootstrap-theme.min.css';
import './assets/css/fullcalendar.css';
import './assets/css/fullcalendar.print.css';
import './assets/css/jasny-bootstrap.min.css';
import './assets/css/navmenu-reveal.css';
import './assets/css/responsive-style.css';

import './assets/css/owl.theme.default.min.css';

import './assets/css/style.css';


import Idn from './assets/img/ind.png';
import logo from './assets/img/logo-long.png';
import eng from './assets/img/eng.png';

function Header() {
    
    return(
        <React.Fragment>

            <div className = "top-header bg-soft-blue col-white">
                <div className = "container">
                    <div className = "row">
                        <div className = "col-md-6 col-xs-12 col-sm-4">
                            <div className = "info-top-header">
                            <i className="fa fa-phone"aria-hidden="true"></i> Hubungi kami: <a href="tel:+622127791412" class="col-white">(021) 2779 1412</a>
                            </div>
                        </div>
                        <div className = "col-md-6 col-xs-12 col-sm-8">
                            <div className = "action-top">
                                    <ul className = "list-item">

                                        <ul className = "list-unstyled">
                                            <i className="fa fa-angle-down col-white" aria-hidden="true"></i>
                                            <li className="init col-white"><img src={Idn} class="img-responsive inline-block"/> IND</li>
                                            
                                            
                                        </ul>
                                    </ul>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="logo">
                <div className="container">
                    <div className="row">
                    <div className="col-md-12 col-xs-12">
                        <div className="image-logo">
                        <a href="#"><img src={logo} alt="BPTJ" className="img-responsive"/></a>
                        </div>
                    </div>
                    </div>
                </div>
                </div>

        </React.Fragment>

);
    }
export default Header