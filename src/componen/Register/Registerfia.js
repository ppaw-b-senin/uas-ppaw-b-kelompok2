import React from 'react';
import Header from './Header';
import Register from './Register';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import './assets/css/bootstrap.min.css';
import './assets/css/bootstrap-theme.min.css';
import './assets/css/fullcalendar.css';
import './assets/css/fullcalendar.print.css';
import './assets/css/jasny-bootstrap.min.css';
import './assets/css/navmenu-reveal.css';
import './assets/css/responsive-style.css';

import './assets/css/owl.theme.default.min.css';

import './assets/css/style.css';

function Registerfia()
{
    return(
        <body>
            <div className="App">
                <Header/>
                <Register/>
            </div>
        </body>
    );
}

export default Registerfia