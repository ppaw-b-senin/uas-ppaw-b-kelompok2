import React from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import './assets/css/bootstrap.min.css';
import './assets/css/bootstrap-theme.min.css';
import './assets/css/fullcalendar.css';
import './assets/css/fullcalendar.print.css';
import './assets/css/jasny-bootstrap.min.css';
import './assets/css/navmenu-reveal.css';
import './assets/css/responsive-style.css';

import './assets/css/owl.theme.default.min.css';

import './assets/css/style.css';

function Register()
{
    return(
        <React.Fragment>
            <div id="inner-login" class="reg-page">
                <div className = "container">
                    <div className = "row">
                        <div className = "col-md-12 col-xs-12 col-sm-12">
                            <div className = "registration-form">
                                <form className="" action="register.html" method="post">
                                    <div className = "header-form">
                                        <h2 className = "title">Registrasi Pemohon</h2>
                                        <p>Untuk aktifasi aplikasi BPTJ Mbile, masukan data diri<br/>Badan Usaha Perusahaan Anda.</p>
                                    </div>
                                    <div className = "badan-usaha">
                                        <p className = "title">1. Badan Usaha</p>
                                        <div className = "form-inline">
                                            <div className = "form-group first-input">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">Nama Badan Usaha</span>
                                            </div>
                                            <div className = "form-group npwp">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">NPWP*</span>
                                            </div>
                                        </div>
                                        <div className = "form-group alamat">
                                            <input type="text" className="form-control input-md inputText" required/>
                                            <span className = "floating-label">Alamat Badan usaha*</span>
                                        </div>
                                        <div className = "form-inline last-inline">
                                            <div className = "form-group">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">Kota*</span>
                                            </div>
                                            <div className = "form-group">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">Provinsi*</span>
                                            </div>
                                            <div className = "form-group">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">Kode Pos*</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div className = "badan-usaha bottom">
                                        <p className = "title">2. Data Pemohon</p>
                                        <div className = "form-group seven">
                                            <input type="text" className="form-control input-md inputText" required/>
                                            <span className="floating-label">KTP/SIM/Passport Pimpinan Perusahaan*</span>
                                        </div>
                                        <div className = "form-group seven">
                                            <input type="text" className="form-control input-md inputText" required/>
                                            <span className="floating-label">Alamat Pimpinan Perusahaan*</span>
                                        </div>
                                        <div className = "form-inline last-inline">
                                            <div className = "form-group">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">Kota*</span>
                                            </div>
                                            <div className = "form-group">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">Provinsi*</span>
                                            </div>
                                            <div className = "form-group last">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">Kode Pos*</span>
                                            </div>
                                        </div>
                                        <div className = "form-inline last-inline">
                                            <div className = "form-group">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">Telepon*</span>
                                            </div>
                                            <div className = "form-group last">
                                                <input type="text" className="form-control input-md inputText" required/>
                                                <span className="floating-label">Email*</span>
                                            </div>
                                        </div>
                                        <div className="form-group half">
                                            <input id="password-field" type="password" className="form-control inputText" name="password" required/>
                                            <span toggle="#password-field" className="fa fa-fw fa-eye field-icon toggle-password"></span>
                                            <span className="floating-label">Password*</span>
                                        </div>
                                        <div className="form-group half">
                                            <input id="password-field2" type="password" className="form-control inputText" name="repeat-password" required/>
                                            <span toggle="#password-field2" className="fa fa-fw fa-eye field-icon toggle-password-2"></span>
                                            <span className="floating-label">Ketik Ulang Password*</span>
                                        </div>
                                    </div>
                                    <div className="checkbox">
                                        <label>
                                        <input type="checkbox"/>I Agree with the Term
                                        </label>
                                    </div>
                                    <div className = "bottom-submit">
                                        <div className = "row">
                                            <div className = "col-md-6 col-xs-12 col-sm-6">
                                                <button type="submit" className="btn btn-default login">REGISTER</button>
                                            </div>
                                            <div className="col-md-6 col-xs-12 col-sm-6">
                                                <div className="g-recaptcha" data-sitekey="6Lf8Hl4UAAAAABJFRwnGlWBaHeWAKK_VIzWs0M-U"></div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className = "footer-home">
                    <div className = "container">
                        <div className = "row">
                            <div className = "col-md-6 col-xs-12 col-sm-6">
                                <div className = "process">
                                    <h3>FLOW PROCESS</h3>
                                    <ul>
                                        <li>
                                            <div className="icon-circle"><i className = "icon icon-employee"></i></div>
                                            <span>Pendaftaran Akun</span>
                                        </li>
                                        <li>
                                            <div className="icon-circle"><i className = "icon icon-doc"></i></div>
                                            <span>Permohonan<br/>Persetujuan Izin</span>
                                        </li>
                                        <li>
                                            <div className="icon-circle"><i className = "icon icon-checklist"></i></div>
                                            <span>Permohonan<br/>Realisasi SK</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div className = "col-md-6 col-xs-12 col-sm-6">
                                <div className = "guide">
                                    <h3>DOWNLOAD USER GUIDE</h3>
                                    <ul>
                                        <li><a><i className="fa fa-caret-right" aria-hidden="true"></i><span className = "text">Pendaftaran Akun</span></a></li>
                                        <li><a><i className="fa fa-caret-right" aria-hidden="true"></i><span className = "text">Permohonan Persetujuan Izin/izin Prinsip</span></a></li>
                                        <li><a><i className="fa fa-caret-right" aria-hidden="true"></i><span className = "text">Permohonan Realisasi SK, Izin Penyelenggaraan dan Kartu Pengawasan</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>

    );
}
export default Register