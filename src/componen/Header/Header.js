import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import '../css/bootstrap.min.css';
import '../css/bootstrap-theme.min.css';
import '../css/fullcalendar.css';
import '../css/fullcalendar.print.css';
import '../css/jasny-bootstrap.min.css';
import '../css/navmenu-reveal.css';
import '../css/responsive-style.css';
import '../css/style.css';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookSquare, faTwitter, faInstagram, faYoutube } from '@fortawesome/free-brands-svg-icons'
import { faPhone } from '@fortawesome/free-solid-svg-icons'

import Ind from '../img/ind.png';
import Eng from '../img/eng.png';
import Logo from '../img/logo-long.png';

export default function Header() {
    return (
        <header>
            <div class="top-header bg-soft-blue col-white">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-xs-12 col-sm-4">
                            <div class="info-top-header">
                                <i class="fa fa-phone" aria-hidden="true"></i><FontAwesomeIcon icon={faPhone} /> Hubungi kami: <a href="tel:+622127791412" class="col-white">(021) 2779 1412</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12 col-sm-8">
                            <div class="action-top">
                                <ul class="list-item">
                                    <li><a href="#" class="col-white"><FontAwesomeIcon icon={faFacebookSquare} /></a></li>
                                    <li><a href="#" class="col-white"><FontAwesomeIcon icon={faTwitter} /></a></li>
                                    <li><a href="#" class="col-white"><FontAwesomeIcon icon={faInstagram} /></a></li>
                                    <li><a href="#" class="col-white"><FontAwesomeIcon icon={faYoutube} /></a></li>
                                    <ul class="list-unstyled">
                                        <i class="fa fa-angle-down col-white" aria-hidden="true"></i>
                                        <li class="init col-white"><img src={Ind} class="img-responsive inline-block" /> IND</li>
                                        <li data-value="Indonesia" class="col-dark ina"><img src={Ind} class="img-responsive inline-block" /> IND</li>
                                        <li data-value="English" class="col-dark eng"><a href="#"><img src={Eng} class="img-responsive inline-block" /> ENG</a></li>
                                    </ul>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="logo">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="image-logo">
                                <a href="#"><img src={Logo} alt="BPTJ" class="img-responsive" />

                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header >
    )
}
