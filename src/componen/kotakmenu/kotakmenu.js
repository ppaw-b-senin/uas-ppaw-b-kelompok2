import React from 'react';
import logo1 from '../assets/img/3---Dashboard---User_izin1.png';
import logo2 from '../assets/img/3---Dashboard---User_izin2.png';
import logo3 from '../assets/img/3---Dashboard---User_izin3.png';
import logo4 from '../assets/img/3---Dashboard---User_izin4.png';
import logo5 from '../assets/img/3---Dashboard---User_izin5.png';
import logo6 from '../assets/img/3---Dashboard---User_izin6.png';
import logo7 from '../assets/img/3---Dashboard---User_izin7.png';
import logo8 from '../assets/img/3---Dashboard---User_izin8.png';
import logo9 from '../assets/img/3---Dashboard---User_izin9.png';
import '../css/bootstrap-theme.css';
import '../css/bootstrap-theme.css.map';
import '../css/bootstrap-theme.min.css';
import '../css/bootstrap-theme.min.css.map';
import '../css/bootstrap.css';
import '../css/bootstrap.css.map';
import '../css/bootstrap.min.css';
import '../css/bootstrap.min.css.map';
import '../App.css';




export default function kotakmenu () {
    return (
            <section id="Daftar" class="daftar">
            <div class="jumbotron mb-0">
               <div class="kotak">
                <div class="row ">
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src={logo1} alt="..."/>
                          <div class="caption">
                            <h3>Permohonan Izin Angkutan Dalam Trayek</h3>
                             <button type="button" class="btn btn-link btn-lg" role="button" data-toggle="modal" data-target="#myModal">DAFTAR</button>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src={logo2} alt="..."/>
                          <div class="caption">
                            <h3>Realisasi SK : Izin Kartu Pengawasan</h3>
                             <button type="button" class="btn btn-link btn-lg" role="button" ><a href="">DAFTAR</a></button>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src={logo3} alt="..."/>
                          <div class="caption">
                            <h3>Penambahan Kartu Pengawasan</h3>
                             <button type="button" class="btn btn-link btn-lg" role="button" ><a href="">DAFTAR</a></button>
                          </div>
                        </div>
                      </div>
                </div>
                <div class="row">
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src={logo4} alt="..."/>
                          <div class="caption">
                            <h3>Penambahan Kendaraan</h3>
                             <button type="button" class="btn btn-link btn-lg" role="button" data-toggle="modal" data-target="#myModal">DAFTAR</button>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src={logo5} alt="..."/>
                          <div class="caption">
                            <h3>Peremajaan Kendaraan</h3>
                             <button type="button" class="btn btn-link btn-lg" role="button" data-toggle="modal" data-target="#myModal">DAFTAR</button>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src={logo6} alt="..."/>
                          <div class="caption">
                            <h3>Penambahan Trayek</h3>
                             <button type="button" class="btn btn-link btn-lg" role="button" data-toggle="modal" data-target="#myModal">DAFTAR</button>
                          </div>
                        </div>
                      </div>
                </div>
                <div class="row">
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src={logo7} alt="..."/>
                          <div class="caption">
                            <h3>Pembaharuan Masa Berlaku</h3>
                             <button type="button" class="btn btn-link btn-lg" role="button" data-toggle="modal" data-target="#myModal">DAFTAR</button>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src={logo8} alt="..."/>
                          <div class="caption">
                            <h3>Perubahan Data Pengurus</h3>
                             <button type="button" class="btn btn-link btn-lg" role="button" data-toggle="modal" data-target="#myModal">DAFTAR</button>
                          </div>
                        </div>
                      </div>
                      <div class="col-sm-6 col-md-4">
                        <div class="thumbnail">
                          <img src={logo9} alt="..."/>
                          <div class="caption">
                            <h3>Penggantian Dokument</h3>
                             <button type="button" class="btn btn-link btn-lg" role="button" data-toggle="modal" data-target="#myModal">DAFTAR</button>
                          </div>
                        </div>
                      </div>
                </div>
              </div>
            </div>
        </section>
    );
}