import React from 'react';
import '../css/bootstrap-theme.css';
import '../css/bootstrap-theme.css.map';
import '../css/bootstrap-theme.min.css';
import '../css/bootstrap-theme.min.css.map';
import '../css/bootstrap.css';
import '../css/bootstrap.css.map';
import '../css/bootstrap.min.css';
import '../css/bootstrap.min.css.map';
import logo from '../assets/img/3---Dashboard---header.png'
import '../App.css';


export default function navbar () {
    return (
    <nav class="mnb navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <div >
                    <a href="#" id="msbo"><i class="ic fa fa-bars"></i> </a><img src={logo} alt="" />
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><i class="fas fa-phone-alt"></i> Hubungi</a></li>
                    <li><a href="#"><i class="fas fa-info-circle"></i> Tentang</a></li>
                    <li><a href="#"><i class="fas fa-question-circle"></i> Bantuan</a></li>
                    <li><a href="#"><i class="fas fa-sign-out-alt"></i> <span class="text">LOGOUT</span></a></li>
                </ul>
            </div>
            
        </div>
    </nav>
    );
}