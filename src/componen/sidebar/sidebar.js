import React from 'react';
import '../css/bootstrap-theme.css';
import '../css/bootstrap-theme.css.map';
import '../css/bootstrap-theme.min.css';
import '../css/bootstrap-theme.min.css.map';
import '../css/bootstrap.css';
import '../css/bootstrap.css.map';
import '../css/bootstrap.min.css';
import '../css/bootstrap.min.css.map';
import logo1 from '../assets/img/Dashboard2.png';
import logo2 from '../assets/img/Dashboard3.png';
import logo3 from '../assets/img/Dashboard4.png';
import logo4 from '../assets/img/Dashboard5.png';
import atas from '../assets/img/Dashboard1.png';
import '../App.css';



export default function sidebar() {
    return (
        <div class="msb" id="msb">
        <nav class="navbar navbar-default" role="navigation">
        <div class="navbar-header">
            <div class="brand-wrapper">
             <div class="navbar-header dashboard">
                <div>
                   <h4>DASHBOARD MENU <a href="#" id="mbo"></a></h4>
                </div>
            </div>
            </div>
        </div>

        <div class="side-menu-container">
            <ul class="nav navbar-nav">
                <button class="dropdown-btn"><h4 > <img src={atas} alt=""/>Perizinan Online<i class="fa fa-caret-down"></i></h4>
                </button>
                  <div class="dropdown-container">
                    <li><a href=""><h4 >Perkotaan</h4></a></li>
                    <li><a href=""><h4 >Pemukiman</h4></a></li>
                  </div>
                <li><a href="#"><h4 ><img src={logo1} alt=""/> Monitoring Berkas</h4></a></li>
                <li><a href="#"><h4 ><img src={logo2} alt=""/> Cara Penggunaan</h4></a></li>
                <li><a href="#"><h4 ><img src={logo3} alt=""/> Call Center</h4></a></li>
                <li><a href="#"><h4 ><img src={logo4} alt=""/> Regulasi/Ketentuan</h4></a></li>
            </ul>
        </div>
    </nav>
   </div>
    );
}